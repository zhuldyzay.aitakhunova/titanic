import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    titles = ["Mr.", "Mrs.", "Miss."]
    res=  []
    for title in titles :
        df_filter = df[df['Name'].str.contains(title, case=False, na=False)]
        missing_vals = df_filter['Age'].isnull().sum()
        medn = df[df_filter['Age'].notnull()]['Age'].median()
        res.append((title, missing_vals,medn))
    return res
